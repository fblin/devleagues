# DevLeagues
symfony 3 bootstrap mysql jquery

Legal :
all rights reserved to Fabien Blin and associates

Contact :
fabienblin.fr

Version 0.3 DevLeagues.com

Changelog 0.3 :
	Created friend table
	Added user to user relations (friend)
	Starting user.add_friend event
	Created more views for Event
	Created mores view for User/Profile

Changelog 0.2 :
  Created new Event form
  Created show Event view
  Bundles :
    StofDoctrineExtensionsBundle

Changelog 0.1 :
  Created Login & Registration Forms with FOSUserBundle
  Created Entities
  Created Controllers
  Created Views
  DataBase :
    Created Entities
  Bundles :
    FOSUserBundle
    DevLeagues
  Assets :
    Bootstrap
    Jquery

LEAVE A COMMENT =)
